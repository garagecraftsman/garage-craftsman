Garage Craftsman is your one-stop shop for all tools, power tool reviews, and comparisons as well as a collection of helpful tutorials covering woodworking, household improvements & maintenance, DIY project ideas and inspiration.

Address: 21 Marshall Court, Taunton, Somerset TA2 6BW 

Phone: +44 7791 050394